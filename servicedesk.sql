-- Adminer 4.7.1 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP DATABASE IF EXISTS `servicerequest`;
CREATE DATABASE `servicerequest` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `servicerequest`;

DROP TABLE IF EXISTS `doc_type`;
CREATE TABLE `doc_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `doc_code` varchar(5) NOT NULL,
  `doc_name` tinytext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `doc_type` (`id`, `doc_code`, `doc_name`) VALUES
(1,	'SPED',	'SPED (SURAT PERMINTAAN EMAIL)'),
(2,	'SPPK',	'SPPK (SURAT PERMINTAAN PERBAIKAN KOMPUTER)'),
(3,	'SPP',	'SPP (SURAT PERMINTAAN PEMBELIAN)'),
(4,	'IOM',	'IOM (INTER OFFICE MEMO)'),
(5,	'OTHER',	'OTHER REQUEST');

DROP TABLE IF EXISTS `employee`;
CREATE TABLE `employee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nik` bigint(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(200) NOT NULL,
  `location` varchar(100) NOT NULL,
  `division` varchar(100) NOT NULL,
  `department` varchar(100) NOT NULL,
  `phone` bigint(225) NOT NULL,
  `email` varchar(100) NOT NULL,
  `dob` date NOT NULL,
  `gender` enum('M','F') NOT NULL,
  `project` json NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `employee` (`id`, `nik`, `first_name`, `last_name`, `location`, `division`, `department`, `phone`, `email`, `dob`, `gender`, `project`) VALUES
(1,	1117827,	'Usman',	'Antharikta',	'Tangerang',	'Hyglenic',	'IT',	21123123,	'uuhusman@gamil.com',	'1994-11-23',	'M',	'[\"ALL\"]'),
(19,	111111,	'Budiyanto',	'',	'Tangerang',	'Hyglenic',	'IT',	0,	'budiyanto@maju-bersama.com',	'2018-05-31',	'M',	'[\"A220003\", \"A220001\"]'),
(22,	1116551,	'ACE',	'KOSASIH',	'Balarajar',	'Energy',	'HSE',	0,	'ace.kosasih@maju-bersama.com',	'0000-00-00',	'M',	'[\"ALL\"]'),
(25,	98765,	'Test',	'',	'Tangerang',	'Energy',	'IT',	0,	'antharikta7@gmail.com',	'0000-00-00',	'M',	'[\"ALL\", \"A220002\", \"A220003\", \"A220001\"]'),
(26,	123466,	'manager',	'it',	'Tangerang',	'Hyglenic',	'IT',	0,	'samator@samator.com',	'2022-07-30',	'M',	'[\"ALL\"]');

DROP TABLE IF EXISTS `list_deparment`;
CREATE TABLE `list_deparment` (
  `id_dept` int(11) NOT NULL AUTO_INCREMENT,
  `name_dept` tinytext NOT NULL,
  PRIMARY KEY (`id_dept`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `list_deparment` (`id_dept`, `name_dept`) VALUES
(1,	'IT'),
(2,	'HSE'),
(3,	'FSO'),
(4,	'WAREHOUSE'),
(5,	'PURCHASING'),
(6,	'ACCOUNTING'),
(7,	'ENGINEERING'),
(8,	'PPC'),
(9,	'PMT'),
(10,	'MARKETING');

DROP TABLE IF EXISTS `log_activity`;
CREATE TABLE `log_activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `request_id` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `close_time` datetime NOT NULL,
  `start_time` datetime NOT NULL,
  `solved_time` datetime NOT NULL,
  `transfer-time` datetime NOT NULL,
  `unsoved_time` datetime NOT NULL,
  `note` text NOT NULL,
  `cancel_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `log_activity` (`id`, `request_id`, `create_time`, `close_time`, `start_time`, `solved_time`, `transfer-time`, `unsoved_time`, `note`, `cancel_time`) VALUES
(25,	1,	'2022-07-11 00:07:28',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'',	'2022-07-14 03:08:36'),
(26,	2,	'2022-07-14 02:07:16',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'',	'0000-00-00 00:00:00'),
(27,	3,	'2022-07-14 02:07:36',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'',	'0000-00-00 00:00:00'),
(28,	4,	'2022-07-14 02:07:53',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'',	'0000-00-00 00:00:00'),
(29,	5,	'2022-07-14 02:07:08',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'',	'0000-00-00 00:00:00'),
(30,	6,	'2022-07-14 02:07:25',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'',	'0000-00-00 00:00:00'),
(31,	7,	'2022-07-14 02:07:40',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'',	'0000-00-00 00:00:00');

DROP TABLE IF EXISTS `notification`;
CREATE TABLE `notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nik_receipt` int(255) NOT NULL,
  `value` text NOT NULL,
  `status` varchar(255) NOT NULL,
  `request_id` int(255) NOT NULL,
  `nik_request` int(255) NOT NULL,
  `times` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `notification` (`id`, `nik_receipt`, `value`, `status`, `request_id`, `nik_request`, `times`) VALUES
(1,	111111,	'New Request',	'unread',	1,	1117827,	'2022-07-11 12:53:28'),
(2,	111111,	'New Request',	'unread',	2,	1117827,	'2022-07-14 02:43:16'),
(3,	1116551,	'New Request',	'unread',	3,	1117827,	'2022-07-14 02:43:36'),
(4,	123466,	'New Request',	'read',	4,	1117827,	'2022-07-14 02:43:53'),
(5,	98765,	'New Request',	'unread',	5,	1117827,	'2022-07-14 02:44:08'),
(6,	1116551,	'New Request',	'unread',	6,	1117827,	'2022-07-14 02:44:25'),
(7,	98765,	'New Request',	'unread',	7,	1117827,	'2022-07-14 02:44:40'),
(8,	111111,	'Canceled',	'unread',	1,	123466,	'2022-07-14 03:08:36');

DROP TABLE IF EXISTS `request`;
CREATE TABLE `request` (
  `id_request` int(255) NOT NULL AUTO_INCREMENT,
  `nik_request` int(255) NOT NULL,
  `nik_receipt` int(255) NOT NULL,
  `dept_req` varchar(25) NOT NULL,
  `dept_rec` varchar(25) NOT NULL,
  `task_id` int(255) NOT NULL,
  `project_code` varchar(10) NOT NULL,
  `order_date` date NOT NULL,
  `close_date` date NOT NULL,
  `status_user` text NOT NULL,
  `start_date` date NOT NULL,
  `finish_date` date NOT NULL,
  `status_pic` text NOT NULL,
  `transfer_from` varchar(11) NOT NULL,
  `pic_note` text NOT NULL,
  PRIMARY KEY (`id_request`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `request` (`id_request`, `nik_request`, `nik_receipt`, `dept_req`, `dept_rec`, `task_id`, `project_code`, `order_date`, `close_date`, `status_user`, `start_date`, `finish_date`, `status_pic`, `transfer_from`, `pic_note`) VALUES
(1,	1117827,	111111,	'IT',	'IT',	1,	'A220001',	'2022-07-11',	'2022-07-14',	'CANCEL',	'0000-00-00',	'0000-00-00',	'unread',	'',	''),
(2,	1117827,	111111,	'IT',	'IT',	2,	'A220002',	'2022-07-14',	'0000-00-00',	'OPEN',	'0000-00-00',	'0000-00-00',	'unread',	'',	''),
(3,	1117827,	1116551,	'IT',	'IT',	3,	'A220001',	'2022-07-14',	'0000-00-00',	'OPEN',	'0000-00-00',	'0000-00-00',	'unread',	'',	''),
(4,	1117827,	123466,	'IT',	'IT',	4,	'ALL',	'2022-07-14',	'0000-00-00',	'OPEN',	'0000-00-00',	'0000-00-00',	'unread',	'',	''),
(5,	1117827,	98765,	'IT',	'IT',	5,	'A220002',	'2022-07-14',	'0000-00-00',	'OPEN',	'0000-00-00',	'0000-00-00',	'unread',	'',	''),
(6,	1117827,	1116551,	'IT',	'IT',	6,	'A220003',	'2022-07-14',	'0000-00-00',	'OPEN',	'0000-00-00',	'0000-00-00',	'unread',	'',	''),
(7,	1117827,	98765,	'IT',	'IT',	7,	'ALL',	'2022-07-14',	'0000-00-00',	'OPEN',	'0000-00-00',	'0000-00-00',	'unread',	'',	'');

DELIMITER ;;

CREATE TRIGGER `request_bi` BEFORE INSERT ON `request` FOR EACH ROW
BEGIN
SET NEW.dept_req = (SELECT department FROM employee WHERE nik - NEW.nik_request limit 1);
SET NEW.dept_rec = (SELECT department FROM employee WHERE nik - NEW.nik_receipt limit 1);
END;;

DELIMITER ;

DROP TABLE IF EXISTS `Task`;
CREATE TABLE `Task` (
  `id_task` int(255) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `project` varchar(255) NOT NULL,
  `task_detail` text NOT NULL,
  `create_date` date NOT NULL,
  `deadline` date NOT NULL,
  `doc_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id_task`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `Task` (`id_task`, `title`, `project`, `task_detail`, `create_date`, `deadline`, `doc_type`) VALUES
(1,	'TEST Create request ',	'A220001',	'<p>tetasvda&nbsp; asbsd yfml /nkswafhos sodf 9[eiwrowrhw</p><p>roki wiwerewrewrewrwrewr</p><p>ewrewrf</p><p>ewr&nbsp;</p><p>wr</p><p>wer</p><p>wer</p>',	'2022-07-11',	'2022-07-30',	'OTHER'),
(2,	'drrgr tee',	'A220002',	'<p>dajhd gwroewrecw w rf]0ewr9uewrh8whrew</p><p>r guwbrtwrg v rnowb&nbsp;</p><p>ew niow[tjwerjeqw</p>',	'2022-07-14',	'2022-07-28',	'SPPK'),
(3,	'awe qaeqeqeqw',	'A220001',	'<p>ewqewq eqweqe qweeqwe qweqweqweqw eqweqwe qweqwe qw</p>',	'2022-07-14',	'2022-07-29',	'SPP'),
(4,	'qwe qweqw',	'ALL',	'<p>qwe q?wewqeqweqweqweEWQEQWEWQEQW</p><p>EQW</p><p>E</p><p>QWE</p><p>QW E</p><p>QW</p><p>EQW</p>',	'2022-07-14',	'2022-08-06',	''),
(5,	'QW EQWEQEQ',	'A220002',	'<p>QWE QWEQWEQ EQEQWEQWE QE</p>',	'2022-07-14',	'2022-07-29',	'SPP'),
(6,	'QWE WQEQE QEWW QEQW',	'A220003',	'<p>EQWEQ WE YQWTVQWETQ67TVEQWUEO8YE9EQ8EGQ</p>',	'2022-07-14',	'2022-07-23',	'SPPK'),
(7,	'QWE QWEQEQE Q',	'ALL',	'<p>QW EQEQEYEQW EQ89EY Q8U E UQ90E QUE</p>',	'2022-07-14',	'2022-08-05',	'');

DROP TABLE IF EXISTS `tbl_project`;
CREATE TABLE `tbl_project` (
  `project_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `project_code` varchar(10) NOT NULL,
  `project_name` tinytext NOT NULL,
  `project_pic` tinytext NOT NULL,
  `project_location` tinytext NOT NULL,
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  `project_detail` text NOT NULL,
  `create_by` varchar(11) NOT NULL,
  `create_date` date NOT NULL,
  `status` enum('open','cancel','done') NOT NULL DEFAULT 'open',
  PRIMARY KEY (`project_id`),
  UNIQUE KEY `project_code` (`project_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `tbl_project` (`project_id`, `project_code`, `project_name`, `project_pic`, `project_location`, `date_start`, `date_end`, `project_detail`, `create_by`, `create_date`, `status`) VALUES
(1,	'ALL',	'ALL',	'ALL',	'ALL',	'0000-00-00',	'0000-00-00',	'',	'',	'0000-00-00',	'open'),
(2,	'A220002',	'PT. RIAU',	'Tutut',	'Riau',	'2022-07-07',	'2022-12-30',	'<p><ol><li>idgaudh-qajwq qhibdqi dqdb qwy8dbqw<br></li><li>eq dfsdfs</li><li>ds fsfsdf sfs fdsfs</li><li>wedsf sf sfds fsdf</li><li>qwedsf sdsf sfdsf ds</li><li>qwsdfs fsfsfdsfds</li><li>eqwfs d f ssds ds</li><li>eqwdfds fs sfsdfsd</li><li>eqws dssdfdsfs</li><li>sfsd fsd sdfsdfds</li></ol></p>',	'1117827',	'2022-07-08',	'open'),
(3,	'A220003',	'PT JAYA BAYA',	'YUYU',	'Jepara',	'2022-07-13',	'2023-03-10',	'<p><ul><li>adhag udha dhadhad</li><li>asdhu agfdasb da&nbsp;</li><li>asdhau dvadnasda</li><li>sd ada asdasda</li><li>da dad asd adas dad as</li><li>dasd</li><li>asd</li><li>asdasd</li><li>asdasdasd</li><li>asdadad&nbsp;</li></ul></p>',	'1117827',	'2022-07-08',	'open'),
(4,	'A220001',	'PT. RIAU ANDALAN PULP AND PAPER',	'uuh',	'riau',	'2022-07-14',	'2022-12-29',	'<p><ol><li>ewr rewrwr rw w</li><li>eww ewwre ewrwe</li><li>rew wrwre ewr wrw</li><li>ewrewew wr ewrewr</li><li>er ew ewrwer ewrw rewr wrwerwe</li><li>wrw rrwr ew rew rwr wrwrw</li><li>ewrewrewr&nbsp;</li></ol></p>',	'1117827',	'0000-00-00',	'open');

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `user_id` int(255) NOT NULL AUTO_INCREMENT,
  `nik` bigint(20) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `level` varchar(255) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `user` (`user_id`, `nik`, `username`, `password`, `level`) VALUES
(1,	1117827,	'1117827',	'$2y$10$z1E2vCYTpyVxQuPPTJfM4.MaofnxvbRNqy2MJPl32evqAEHR/2ljG',	'admin'),
(13,	111111,	'111111',	'$2y$10$JAMTs/UiVxkSLl52pSV1IOeirCEawADNkPCZZjvHmqWv4v70gTzJO',	'admin'),
(14,	98765,	'98765',	'$2y$10$pRVHW5zfkLYwiIIJfxTuteIp3t9zU8dJ0kytGIsj.PhmQgnPC8AlK',	'staf'),
(15,	123466,	'123466',	'$2y$10$TLsBIvXcyS2vBjW7B3ud/OxOwgAkVe8U.FGxAlRh47ZQ9c69c8W/i',	'manager');

-- 2022-12-01 01:44:16
