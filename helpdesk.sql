-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 28, 2017 at 05:31 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `helpdesk`
--

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `id` int(11) NOT NULL,
  `nik` bigint(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(200) NOT NULL,
  `location` varchar(100) NOT NULL,
  `division` varchar(100) NOT NULL,
  `department` varchar(100) NOT NULL,
  `phone` bigint(225) NOT NULL,
  `email` varchar(100) NOT NULL,
  `dob` date NOT NULL,
  `gender` enum('M','F') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`id`, `nik`, `first_name`, `last_name`, `location`, `division`, `department`, `phone`, `email`, `dob`, `gender`) VALUES
(1, 1117827, 'Usman', 'Antharikta', 'Tangerang', 'Hyglenic', 'IT', 21123123, 'uuhusman@gamil.com', '1994-11-23', 'M'),
(12, 112345, 'General ', 'Manager', 'Tangerang', 'Hyglenic', 'IT', 2343234324242423, 'general_manager@maju-bersama.com', '1989-12-07', 'M'),
(13, 12312414, 'Staff', 'Satu', 'Balarajar', 'Energy', 'IT', 0, 'staf1@maju-bersama.com', '1889-10-30', 'F'),
(14, 23424234, 'staf', 'dua', 'Balarajar', 'Chemical', 'IT', 3242, 'staf2@maju-bersama.com', '1979-02-06', 'F'),
(15, 213213213, 'Directure', 'Utama', 'Tangerang', 'Hyglenic', 'IT', 342342423, 'directure@maju-bersama.com', '1790-12-01', 'M'),
(16, 13412, 'Super', 'Admin', 'Tangerang', 'Energy', 'IT', 134124141, 'admin@maju-bersama.com', '2017-06-05', 'M'),
(17, 123456, 'staff', 'tiga', 'Tangerang', 'Energy', 'IT', 87654321, 'tiga@mb.com', '1985-11-28', 'M'),
(18, 34567, 'Staf', 'test', 'Tangerang', 'Hyglenic', 'IT', 98765432, 'sdfs@sdsdfs.com', '1988-12-26', 'F');

-- --------------------------------------------------------

--
-- Table structure for table `log_activity`
--

CREATE TABLE `log_activity` (
  `id` int(11) NOT NULL,
  `request_id` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `close_time` datetime NOT NULL,
  `start_time` datetime NOT NULL,
  `solved_time` datetime NOT NULL,
  `transfer-time` datetime NOT NULL,
  `unsoved_time` datetime NOT NULL,
  `note` text NOT NULL,
  `cancel_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `log_activity`
--

INSERT INTO `log_activity` (`id`, `request_id`, `create_time`, `close_time`, `start_time`, `solved_time`, `transfer-time`, `unsoved_time`, `note`, `cancel_time`) VALUES
(273, 412, '2017-11-27 10:11:08', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-11-28 03:35:43', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(274, 413, '2017-11-27 10:11:25', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(275, 414, '2017-11-27 10:11:41', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(276, 415, '2017-11-27 10:11:15', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(277, 416, '2017-11-28 02:11:41', '0000-00-00 00:00:00', '2017-11-28 03:14:11', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(278, 417, '2017-11-28 02:11:06', '0000-00-00 00:00:00', '2017-11-28 03:13:57', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(279, 418, '2017-11-28 02:11:18', '0000-00-00 00:00:00', '2017-11-28 03:13:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(280, 419, '2017-11-28 02:11:04', '0000-00-00 00:00:00', '2017-11-28 03:12:38', '2017-11-28 03:13:01', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(281, 420, '2017-11-28 02:11:44', '0000-00-00 00:00:00', '2017-11-28 03:11:39', '2017-11-28 03:12:14', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(282, 421, '2017-11-28 02:11:51', '0000-00-00 00:00:00', '2017-11-28 03:11:05', '2017-11-28 03:11:21', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(283, 422, '2017-11-28 02:11:14', '0000-00-00 00:00:00', '2017-11-28 03:10:28', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-11-28 03:10:47', '', '0000-00-00 00:00:00'),
(284, 423, '2017-11-28 02:11:31', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '2017-11-28 03:09:03'),
(285, 424, '2017-11-28 02:11:54', '2017-11-28 03:09:46', '2017-11-28 03:08:27', '2017-11-28 03:08:40', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(286, 425, '2017-11-28 02:11:16', '2017-11-28 03:09:34', '2017-11-28 03:07:12', '2017-11-28 03:07:23', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(287, 426, '2017-11-28 02:11:26', '2017-11-28 03:09:22', '2017-11-28 02:58:25', '2017-11-28 02:59:37', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(288, 427, '2017-11-28 02:11:36', '2017-11-28 03:09:15', '2017-11-28 02:58:39', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-11-28 02:58:49', '', '0000-00-00 00:00:00'),
(289, 428, '2017-11-28 03:11:55', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(290, 429, '2017-11-28 03:11:04', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(291, 430, '2017-11-28 03:11:58', '0000-00-00 00:00:00', '2017-11-28 03:50:46', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(292, 431, '2017-11-28 03:11:10', '0000-00-00 00:00:00', '2017-11-28 03:48:44', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(293, 432, '2017-11-28 03:11:19', '0000-00-00 00:00:00', '2017-11-28 03:48:28', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(294, 433, '2017-11-28 03:11:00', '0000-00-00 00:00:00', '2017-11-28 03:48:01', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-11-28 03:48:10', '', '0000-00-00 00:00:00'),
(295, 434, '2017-11-28 03:11:06', '0000-00-00 00:00:00', '2017-11-28 03:47:30', '2017-11-28 03:47:44', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(296, 435, '2017-11-28 03:11:16', '0000-00-00 00:00:00', '2017-11-28 03:46:31', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-11-28 03:46:42', '', '0000-00-00 00:00:00'),
(297, 436, '2017-11-28 03:11:24', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '2017-11-28 03:46:11'),
(298, 437, '2017-11-28 03:11:37', '2017-11-28 03:52:06', '2017-11-28 03:45:20', '2017-11-28 03:45:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(299, 438, '2017-11-28 03:11:44', '2017-11-28 03:52:12', '2017-11-28 03:44:52', '2017-11-28 03:45:03', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(300, 439, '2017-11-28 03:11:58', '2017-11-28 03:51:22', '2017-11-28 03:44:20', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-11-28 03:44:31', '', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE `notification` (
  `id` int(11) NOT NULL,
  `nik_receipt` int(255) NOT NULL,
  `value` text NOT NULL,
  `status` varchar(255) NOT NULL,
  `request_id` int(255) NOT NULL,
  `nik_request` int(255) NOT NULL,
  `times` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notification`
--

INSERT INTO `notification` (`id`, `nik_receipt`, `value`, `status`, `request_id`, `nik_request`, `times`) VALUES
(186, 1117827, 'New Request', 'unread', 412, 34567, '2017-11-27 10:21:08'),
(187, 1117827, 'New Request', 'unread', 413, 34567, '2017-11-27 10:21:25'),
(188, 1117827, 'New Request', 'unread', 414, 34567, '2017-11-27 10:21:41'),
(189, 1117827, 'New Request', 'unread', 415, 34567, '2017-11-27 10:22:15'),
(190, 112345, 'New Request', 'unread', 416, 34567, '2017-11-28 02:50:41'),
(191, 112345, 'New Request', 'unread', 417, 34567, '2017-11-28 02:51:06'),
(192, 112345, 'New Request', 'unread', 418, 34567, '2017-11-28 02:51:18'),
(193, 112345, 'New Request', 'unread', 419, 34567, '2017-11-28 02:52:05'),
(194, 112345, 'New Request', 'unread', 420, 34567, '2017-11-28 02:52:44'),
(195, 112345, 'New Request', 'unread', 421, 34567, '2017-11-28 02:52:51'),
(196, 112345, 'New Request', 'unread', 422, 34567, '2017-11-28 02:53:14'),
(197, 112345, 'New Request', 'unread', 423, 34567, '2017-11-28 02:53:31'),
(198, 112345, 'New Request', 'unread', 424, 34567, '2017-11-28 02:53:54'),
(199, 112345, 'New Request', 'unread', 425, 34567, '2017-11-28 02:54:16'),
(200, 112345, 'New Request', 'unread', 426, 34567, '2017-11-28 02:54:27'),
(201, 112345, 'New Request', 'read', 427, 34567, '2017-11-28 02:54:36'),
(202, 34567, 'Unsolved', 'unread', 427, 112345, '2017-11-28 02:58:49'),
(203, 34567, 'Solved', 'unread', 426, 112345, '2017-11-28 02:59:37'),
(204, 34567, 'Solved', 'read', 425, 112345, '2017-11-28 03:07:23'),
(205, 34567, 'Solved', 'unread', 424, 112345, '2017-11-28 03:08:40'),
(206, 112345, 'Canceled', 'unread', 423, 34567, '2017-11-28 03:09:03'),
(207, 34567, 'Unsolved', 'read', 422, 112345, '2017-11-28 03:10:47'),
(208, 34567, 'Solved', 'unread', 421, 112345, '2017-11-28 03:11:21'),
(209, 34567, 'Solved', 'unread', 420, 112345, '2017-11-28 03:12:14'),
(210, 34567, 'Solved', 'read', 419, 112345, '2017-11-28 03:13:01'),
(211, 34567, 'New Request', 'read', 428, 112345, '2017-11-28 03:40:55'),
(212, 34567, 'New Request', 'read', 429, 112345, '2017-11-28 03:41:04'),
(213, 34567, 'New Request', 'read', 430, 112345, '2017-11-28 03:41:58'),
(214, 34567, 'New Request', 'read', 431, 112345, '2017-11-28 03:42:10'),
(215, 34567, 'New Request', 'read', 432, 112345, '2017-11-28 03:42:19'),
(216, 34567, 'New Request', 'read', 433, 112345, '2017-11-28 03:43:00'),
(217, 34567, 'New Request', 'read', 434, 112345, '2017-11-28 03:43:06'),
(218, 34567, 'New Request', 'read', 435, 112345, '2017-11-28 03:43:16'),
(219, 34567, 'New Request', 'read', 436, 112345, '2017-11-28 03:43:24'),
(220, 34567, 'New Request', 'read', 437, 112345, '2017-11-28 03:43:37'),
(221, 34567, 'New Request', 'read', 438, 112345, '2017-11-28 03:43:44'),
(222, 34567, 'New Request', 'read', 439, 112345, '2017-11-28 03:43:58'),
(223, 112345, 'Unsolved', 'read', 439, 34567, '2017-11-28 03:44:31'),
(224, 112345, 'Solved', 'unread', 438, 34567, '2017-11-28 03:45:03'),
(225, 112345, 'Solved', 'unread', 437, 34567, '2017-11-28 03:45:30'),
(226, 34567, 'Canceled', 'read', 436, 112345, '2017-11-28 03:46:12'),
(227, 112345, 'Unsolved', 'unread', 435, 34567, '2017-11-28 03:46:42'),
(228, 112345, 'Solved', 'unread', 434, 34567, '2017-11-28 03:47:44'),
(229, 112345, 'Unsolved', 'unread', 433, 34567, '2017-11-28 03:48:11');

-- --------------------------------------------------------

--
-- Table structure for table `request`
--

CREATE TABLE `request` (
  `id_request` int(255) NOT NULL,
  `nik_request` int(255) NOT NULL,
  `nik_receipt` int(255) NOT NULL,
  `task_id` int(255) NOT NULL,
  `order_date` date NOT NULL,
  `close_date` date NOT NULL,
  `status_user` text NOT NULL,
  `start_date` date NOT NULL,
  `finish_date` date NOT NULL,
  `status_pic` text NOT NULL,
  `transfer_from` varchar(11) NOT NULL,
  `pic_note` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `request`
--

INSERT INTO `request` (`id_request`, `nik_request`, `nik_receipt`, `task_id`, `order_date`, `close_date`, `status_user`, `start_date`, `finish_date`, `status_pic`, `transfer_from`, `pic_note`) VALUES
(412, 34567, 34567, 382, '2017-11-27', '0000-00-00', 'OPEN', '0000-00-00', '0000-00-00', 'unread', '34567', ''),
(413, 34567, 1117827, 383, '2017-11-27', '0000-00-00', 'OPEN', '0000-00-00', '0000-00-00', 'unread', '', ''),
(414, 34567, 1117827, 384, '2017-11-27', '0000-00-00', 'OPEN', '0000-00-00', '0000-00-00', 'unread', '', ''),
(415, 34567, 1117827, 385, '2017-11-27', '0000-00-00', 'OPEN', '0000-00-00', '0000-00-00', 'unread', '', ''),
(416, 34567, 112345, 386, '2017-11-28', '0000-00-00', 'OPEN', '2017-11-28', '0000-00-00', 'onprogress', '', ''),
(417, 34567, 112345, 387, '2017-11-28', '0000-00-00', 'OPEN', '2017-11-28', '0000-00-00', 'onprogress', '', ''),
(418, 34567, 112345, 388, '2017-11-28', '0000-00-00', 'OPEN', '2017-11-30', '0000-00-00', 'onprogress', '', ''),
(419, 34567, 112345, 389, '2017-11-28', '0000-00-00', 'OPEN', '2017-11-28', '2017-11-30', 'solved', '', ''),
(420, 34567, 112345, 390, '2017-11-28', '0000-00-00', 'OPEN', '2017-11-28', '2017-11-28', 'solved', '', ''),
(421, 34567, 112345, 391, '2017-11-28', '0000-00-00', 'OPEN', '2017-11-28', '2017-11-28', 'solved', '', ''),
(422, 34567, 112345, 392, '2017-11-28', '0000-00-00', 'OPEN', '2017-11-28', '2017-11-30', 'unsolved', '', ''),
(423, 34567, 112345, 393, '2017-11-28', '2017-11-28', 'CANCEL', '0000-00-00', '0000-00-00', 'unread', '', ''),
(424, 34567, 112345, 394, '2017-11-28', '2017-11-28', 'CLOSE', '2017-11-28', '2017-11-29', 'solved', '', ''),
(425, 34567, 112345, 395, '2017-11-28', '2017-11-28', 'CLOSE', '2017-11-28', '2017-11-30', 'solved', '', ''),
(426, 34567, 112345, 396, '2017-11-28', '2017-11-28', 'CLOSE', '2017-11-28', '2017-12-12', 'solved', '', ''),
(427, 34567, 112345, 397, '2017-11-28', '2017-11-28', 'CLOSE', '2017-11-28', '2017-11-29', 'unsolved', '', ''),
(428, 112345, 34567, 398, '2017-11-28', '0000-00-00', 'OPEN', '0000-00-00', '0000-00-00', 'unread', '', ''),
(429, 112345, 34567, 399, '2017-11-28', '0000-00-00', 'OPEN', '0000-00-00', '0000-00-00', 'unread', '', ''),
(430, 112345, 34567, 400, '2017-11-28', '0000-00-00', 'OPEN', '2017-11-28', '0000-00-00', 'onprogress', '', ''),
(431, 112345, 34567, 401, '2017-11-28', '0000-00-00', 'OPEN', '2017-11-28', '0000-00-00', 'onprogress', '', ''),
(432, 112345, 34567, 402, '2017-11-28', '0000-00-00', 'OPEN', '2017-11-28', '0000-00-00', 'onprogress', '', ''),
(433, 112345, 34567, 403, '2017-11-28', '0000-00-00', 'OPEN', '2017-11-28', '2017-11-30', 'unsolved', '', ''),
(434, 112345, 34567, 404, '2017-11-28', '0000-00-00', 'OPEN', '2017-11-28', '2017-11-28', 'solved', '', ''),
(435, 112345, 34567, 405, '2017-11-28', '0000-00-00', 'OPEN', '2017-11-28', '2017-11-29', 'unsolved', '', ''),
(436, 112345, 34567, 406, '2017-11-28', '2017-11-28', 'CANCEL', '0000-00-00', '0000-00-00', 'unread', '', ''),
(437, 112345, 34567, 407, '2017-11-28', '2017-11-28', 'CLOSE', '2017-11-28', '2017-11-28', 'solved', '', ''),
(438, 112345, 34567, 408, '2017-11-28', '2017-11-28', 'CLOSE', '2017-11-28', '2017-12-09', 'solved', '', ''),
(439, 112345, 34567, 409, '2017-11-28', '2017-11-28', 'CLOSE', '2017-11-29', '2017-11-30', 'unsolved', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `Task`
--

CREATE TABLE `Task` (
  `id_task` int(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `task_detail` text NOT NULL,
  `create_date` date NOT NULL,
  `deadline` date NOT NULL,
  `doc_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Task`
--

INSERT INTO `Task` (`id_task`, `title`, `task_detail`, `create_date`, `deadline`, `doc_type`) VALUES
(382, 'open-unread-normal', 'ffs', '2017-11-27', '2018-01-06', 'SPED'),
(383, 'open-unread-near', 'ffs', '2017-11-27', '2017-11-29', 'SPED'),
(384, 'open-unread-miss', 'ffs', '2017-11-27', '2017-11-14', 'SPED'),
(385, 'open-unread-near', '', '2017-11-27', '2017-11-28', 'SPED'),
(386, 'onprogress-normal', 'onprogress-normal', '2017-11-28', '2017-12-09', 'IOM'),
(387, 'onprogress-mendekati', 'onprogress', '2017-11-28', '2017-11-29', 'IOM'),
(388, 'onprogress-lewat', 'onprogress', '2017-11-28', '2017-11-15', 'IOM'),
(389, 'solved-lewat', 'solved', '2017-11-28', '2017-11-17', 'IOM'),
(390, 'solved-mendekati', 'solved', '2017-11-28', '2017-11-28', 'IOM'),
(391, 'solved-normal', 'solved', '2017-11-28', '2017-11-28', 'IOM'),
(392, 'unsolved', 'unsolvedsolved', '2017-11-28', '2017-11-28', 'IOM'),
(393, 'cancel', 'cancel', '2017-11-28', '2017-11-28', 'IOM'),
(394, 'close-normal', 'close-', '2017-11-28', '2017-11-30', 'IOM'),
(395, 'close-solved-normal', 'close-', '2017-11-28', '2017-12-09', 'IOM'),
(396, 'close-solved-lewat', 'close-', '2017-11-28', '2017-12-09', 'IOM'),
(397, 'close-unsolved', 'close-', '2017-11-28', '2017-12-09', 'IOM'),
(398, 'open-unread-neear', '', '2017-11-28', '2017-11-29', 'SPP'),
(399, 'open-unread-miss', '', '2017-11-28', '2017-11-22', 'SPP'),
(400, 'open-onprogress-norm', '', '2017-11-28', '2017-12-09', 'SPP'),
(401, 'open-onprogress-near', '', '2017-11-28', '2017-11-29', 'SPP'),
(402, 'open-onprogress-miss', '', '2017-11-28', '2017-11-23', 'SPP'),
(403, 'open-solved-miss', '', '2017-11-28', '2017-11-29', 'SPP'),
(404, 'open-solved-normal', '', '2017-11-28', '2017-11-29', 'SPP'),
(405, 'open-unsolved', '', '2017-11-28', '2017-11-29', 'SPP'),
(406, 'cancel', '', '2017-11-28', '2017-11-29', 'SPP'),
(407, 'close-solved-normal', '', '2017-11-28', '2017-11-29', 'SPP'),
(408, 'close-solved-miss', '', '2017-11-28', '2017-11-29', 'SPP'),
(409, 'close-unsolved', '', '2017-11-28', '2017-11-29', 'SPP');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(255) NOT NULL,
  `nik` bigint(20) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `level` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `nik`, `username`, `password`, `level`) VALUES
(1, 1117827, '1117827', '$2y$10$z1E2vCYTpyVxQuPPTJfM4.MaofnxvbRNqy2MJPl32evqAEHR/2ljG', 'admin'),
(7, 112345, '112345', '$2y$10$82yfq4JgIuNnq65.5Qx2BOGH2zbM20MZtXnKOO6JvqP3QGU1Pk5qK', 'manager'),
(8, 12312414, '12312414', '$2y$10$.tS09BZFSHtHOGhspE0A/OHwXv17QP/pBdz6wfIpBDpwCD0sY7GW6', 'staf'),
(9, 23424234, '23424234', '$2y$10$FgcBK4g9Fak4fE6Js3U.zuqaGxbN1IeYEU41rjAlVWmWjpYSopXha', 'staf'),
(10, 213213213, '213213213', '$2y$10$X.vyzA.Ep4T3M4TB4P5ffOcFv8KQpaLo.YijYiJn94SrNoAEHUr8m', 'directure'),
(11, 13412, '13412', '$2y$10$a2bTSz493ZNZqyj8IUi5eeKlzuynYC4I2ay/b4AEUhoOR9TvDsLMu', 'admin'),
(12, 34567, '34567', '$2y$10$mJgat1pn8mHFzHU/H7SedOAuQSXbyYb6o3yThCmU/M.rXiEm4c4.u', 'staf');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `log_activity`
--
ALTER TABLE `log_activity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `request`
--
ALTER TABLE `request`
  ADD PRIMARY KEY (`id_request`);

--
-- Indexes for table `Task`
--
ALTER TABLE `Task`
  ADD PRIMARY KEY (`id_task`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `log_activity`
--
ALTER TABLE `log_activity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=301;

--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=230;

--
-- AUTO_INCREMENT for table `request`
--
ALTER TABLE `request`
  MODIFY `id_request` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=440;

--
-- AUTO_INCREMENT for table `Task`
--
ALTER TABLE `Task`
  MODIFY `id_task` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=410;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
