
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Service Request</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

<!-- load css  -->
<?php $this->load->view('include/css');?>
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<style>
/*.control-label{
  text-align: left;
}*/
/*.form-horizontal .control-label {
    text-align: left;
}*/
.table>thead:first-child>tr:first-child>th {
    border-top: 0;
    text-align: center;
}

</style>
<!-- ADD THE CLASS fixed TO GET A FIXED HEADER AND SIDEBAR LAYOUT -->
<!-- the fixed layout is not compatible with sidebar-mini -->
<body class="hold-transition skin-blue fixed sidebar-mini sidebar-mini-expand-feature sidebar-collapse">
  <?php
  $a=true;
  if($a){

?>
<!-- Site wrapper -->
<div class="wrapper">

  <!-- load header -->
  <?php $this->load->view('include/header_report');?>
  <!-- =============================================== -->
  <!-- load asside  -->
  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Receipt Task List
      </h1>
      <!-- <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Receipt</a></li>
        <li class="active">list</li>
      </ol> -->
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- <div class="callout callout-info">
        <h4>Tip!</h4>

        <p>Add the fixed class to the body tag to get this layout. The fixed layout is your best option if your sidebar
          is bigger than your content because it prevents extra unwanted scrolling.</p>
      </div> -->
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">Filter</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <!-- <?php var_dump($filter_data);?> -->
<!-- form start -->
<form  id='filter-form' class="form-horizontal" action="<?php echo site_url().'/test'?>" method="get">
  <input type="hidden" name='search' >
  <div class="col-lg-6">
    <div class="form-group">
      <label for="inputPassword3"  class="col-sm-2 control-label">Asset type</label>
      <div class="col-sm-10">
        <select id='nik' name="asset_type" class="form-control select2" style="width: 100%;">
          <option value=""> Select one Recipient</option>
          <?php
          foreach ($asset as $key) {
            echo '<option value="'.$key['AssetTypeID'].'"> '.$key['AssetTypeName'].'</option>';
          }
          ?>
        </select>
      </div>
    </div>
    <div class="form-group">
      <label for="inputPassword3" class="col-sm-2 control-label">Location</label>
      <div class="col-sm-10">
        <!-- <input type="text" name="status_user" class="form-control" id="inputPassword3" placeholder="Status User"> -->
        <select name="location" class="form-control select2" style="width: 100%;">
          <option value="">Select One</option>
          <?php
          foreach ($location as $key) {
            echo '<option value="'.$key['LocID'].'"> '.$key['LocName'].'</option>';
          }
          ?>
        </select>
      </div>
    </div>
  </div>
  </form>
  <div style="border-top: 1px solid #ffffff" class="box-footer">
    <button onclick="reset_fo()" class="btn pull-right btn-warning">Reset</button>
    <button onclick="submit()" class="btn btn-info pull-right">Filter</button>
    <button onclick="export_file()" class="btn btn-primary pull-right">Export</button>

  </div>
  <!-- /.box-footer -->
        </div>
        <!-- /.box-body -->
      </div>
      <!-- Default box -->
      <div class="box result_view">
        <div class="box-header with-border">
          <h3 class="box-title">View Receipt</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body table-responsive">
          <!-- <button class="btn btn-primary" onclick="reload_table()">Reload</button> -->
          <!-- <h3>Toggle Column View</h3>
          <div class="btn-group">
            <button class="toggle-vis btn btn-flat btn-info" data-column='0'>NIK User</button>
            <button class="toggle-vis btn btn-flat btn-info" data-column='1'>Name User</button>
            <button class="toggle-vis btn btn-flat btn-info" data-column='2'>Division User</button>
            <button class="toggle-vis btn btn-flat btn-info" data-column='3'>NIK PIC</button>
            <button class="toggle-vis btn btn-flat btn-info" data-column='4'>Name PIC</button>
            <button class="toggle-vis btn btn-flat btn-info" data-column='5'>Division PIC</button>
            <button class="toggle-vis btn btn-flat btn-info" data-column='8'>Doc Type</button>
            <button class="toggle-vis btn btn-flat btn-info" data-column='16'>Transfer From</button>
          </div>
        </br>
        <br> -->
          <table id="table_report" class="table table-hover table-bordered">
            <thead >
              <!-- <tr style="text-align: center">
                <th colspan="3">User</th>
                <th colspan="3">PIC</th>
                <th colspan="11">Task Detail</th>
              </tr> -->
              <tr>
                <!-- <th>NoBukti</th> -->
                <th>AssetID</th>
                <th>AssetName </th>
                <th>NoSeri</th>
                <th>Type</th>
                <th>Merk</th>
                <th>Loc ID</th>
                <th>Loc Name</th>
                <!-- <th>Qty</th> -->
                <th>Tangal Bukti</th>
                <!-- <th>Action </th> -->
              </tr>
            </thead>
            <tbody>
              <?php echo $html;?>
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- footer on here -->
  <?php $this->load->view('include/footer')?>

  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php
  }else{
    echo 'you are not login';
  }
   ?>
<!-- load js ------------------------------------------------------------------>
<?php $this->load->view('include/js');?>
<script src="<?php echo base_url().'assets/moment/moment.js'?>"></script>
<script>
    moment().format();
</script>
<script>
var table;
var save_status='';
$(document).ready(function(){
  $("#list").addClass('active');
  $("#list").parent().parent().addClass('active menu-open');
  // parent().parent().addClass('active');
  table = $('#table_report').DataTable({
    "order": [[ 1, "desc" ]],
    // scrollY:        "700px",
    // scrollX:        true,
    // scrollCollapse: true,
    // // paging:         false,
    // fixedColumns:   {
    //     leftColumns: 0,
    //     rightColumns: 3
    // }
  });

  //Initialize Select2 Elements
  $('.select2').select2({
    placeholder: "Please Select One",
    allowClear: true
  });

});

// GetCellValues();
function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax
}

function submit()
{
  $('[name="search"]').val('yes');
  $('#filter-form').submit();
}

function export_file()
{
  $('[name="search"]').val('yes');
  console.log('test: '+$('[name="export"]').val());
  $('#filter-form').submit();
}


</script>
</body>
</html>
