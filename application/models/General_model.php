<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* this class using for genral propose
*/

class General_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        // $CI = &get_instance();
        // $this->mssql = $CI->load->database('dbsqlsrv',TRUE);
        // $this->db2= $CI->load->database('po', TRUE); // pembelian database
        // $this->access= $CI->load->database('access', TRUE);

    }

    public function insert_access($table, $data){
        $this->access->insert($table, $data);
        return $this->access->insert_id();
    }

    public function select_access($sql, $type=null)
    {
        $result=$this->access->query($sql);
        if($type==null){
            return $result->row_array();
        }
        elseif($type == 'result') {
            return $result->result_array();
        }
    }

    public function select_db2($sql)
    {
        $result=$this->db2->query($sql);
        return $result->result_array();
    }

    public function test(){
        // $sql="select * from Pengguna";
        // $result=$this->mssql->query($sql);
        // return $result->result_array();
        $this->mssql->update('Pengguna' ,array('password'=>password_hash("engineering",  PASSWORD_DEFAULT)), array('user_id'=>13));
        return $this->db->affected_rows();
    }

    public function insert_ms($table, $data){
        $this->mssql->insert($table, $data);
        return $this->mssql->insert_id();
    }

    public function insert($table, $data){
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }
    public function insert_batch($table, $array){
        $this->db->insert_batch($table, $array);
        return true;
    }
    public function update($table, $data, $where){
        $this->db->update($table ,$data, $where);
        return $this->db->affected_rows();
    }

    public function update_batch($table, $array, $where){
        $this->db->update_batch($table ,$array, $where);
        return true;
    }

    public function select($sql){
        $result=$this->db->query($sql);
        return $result->result_array();
    }

    public function select_row($sql){
        $result=$this->db->query($sql);
        return $result->row_array();
    }

    public function delete($id,$colom, $table)
    {
        $this->db->where($colom, $id);
        $this->db->delete($table);
    }

    public function update_manual($sql)
    {
        $update=$this->db->query($sql);
        return $update;
    }

    public function get_counter($code){
        $sql = "SELECT * FROM counter WHERE code='$code'";
        $query=$this->db->query($sql);
        $result = $query->row_array();
        return $result['kepala'].str_pad($result['hitung'], $result['jum_angka'], '0', STR_PAD_LEFT);
    }
    public function set_counter($code,$plus=1){
        $sql = "UPDATE counter
        SET hitung = hitung + $plus
        WHERE code='$code'";
        $result=$this->db->query($sql);
        return $result;
    }

    private function penyebut($nilai) {
        $nilai = abs($nilai);
        $huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
        $temp = "";
        if ($nilai < 12) {
            $temp = " ". $huruf[$nilai];
        } else if ($nilai <20) {
            $temp = $this->penyebut($nilai - 10). " belas";
        } else if ($nilai < 100) {
            $temp = $this->penyebut($nilai/10)." plh". $this->penyebut($nilai % 10);
        } else if ($nilai < 200) {
            $temp = " seratus" . $this->penyebut($nilai - 100);
        } else if ($nilai < 1000) {
            $temp = $this->penyebut($nilai/100) . " ratus" . $this->penyebut($nilai % 100);
        } else if ($nilai < 2000) {
            $temp = " seribu" . $this->penyebut($nilai - 1000);
        } else if ($nilai < 1000000) {
            $temp = $this->penyebut($nilai/1000) . " ribu" . $this->penyebut($nilai % 1000);
        } else if ($nilai < 1000000000) {
            $temp = $this->penyebut($nilai/1000000) . " juta" . $this->penyebut($nilai % 1000000);
        } else if ($nilai < 1000000000000) {
            $temp = $this->penyebut($nilai/1000000000) . " milyar" . $this->penyebut(fmod($nilai,1000000000));
        } else if ($nilai < 1000000000000000) {
            $temp = $this->penyebut($nilai/1000000000000) . " trilyun" . $this->penyebut(fmod($nilai,1000000000000));
        }
        return $temp;
    }

    function terbilang($nilai) {
        if($nilai<0) {
            $hasil = "minus ". trim($this->penyebut($nilai));
        } else {
            $hasil = trim($this->penyebut($nilai));
        }
        return $hasil;
    }

}
