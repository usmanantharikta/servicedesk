<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends CI_Controller {

	/**
	 * This controller use for access to application like
	 * view login
	 */

	public function __construct()
	{
		parent::__construct();
		// $this->load->model('Test_model');
	}

	public function index()
	{
		$search=$this->input->get('search');
		$asset_type=$this->input->get('asset_type');
		$location=$this->input->get('location');

		if($search=='yes'){
			// $data['html']=$this->get_data($asset_type, $location);
			$data['html']=$this->get_data($asset_type, $location);
		}
		else{
			$data['html']='';
		}

		$data['asset']=$this->get_assets();
		$data['location']=$this->get_location();

		$this->load->view('Assets/report', $data);
	}

	function get_row($NoBukti, $asset_type, $location){
	  $dbName = 'E:/Asset Management/ModuleMtn.mdb';
	  if (!file_exists($dbName)) {
	      die("Could not find database file.");
	  }

	  $db = new PDO("odbc:DRIVER={Microsoft Access Driver (*.mdb)}; DBQ=$dbName; Uid=; Pwd=;");
	  // $sql  = "SELECT tblMutasi.NoBukti, tblAsset.AssetID, tblAsset.AssetName, tblAsset.NoSeri, tblAsset.Type, tblAsset.Merk, tblMutasiDetail.NamaKary, tblAsset.CheckOut, tblMutasiDetail.LocID, tblMutasiDetail.LocName, tblMutasiDetail.Sat, tblMutasiDetail.Qty, tblMutasiDetail.Note, tblMutasi.TglBukti
	  // FROM tblMutasi INNER JOIN (tblMutasiDetail INNER JOIN tblAsset ON tblMutasiDetail.AssetID=tblAsset.AssetID) ON tblMutasi.NoBukti=tblMutasiDetail.NoBukti
	  // WHERE (((tblMutasi.TglBukti)=(SELECT Max(tblMutasi.TglBukti) FROM tblMutasi where tblMutasi.NoBukti='$NoBukti')) ";
		// $sql="SELECT * FROM tblAsset where AssetTypeID=2";

		// $sql="SELECT tblMutasi.NoBukti, tblAsset.AssetID, tblAsset.AssetName, tblAsset.NoSeri, tblAsset.Type, tblAsset.Merk, tblMutasiDetail.NamaKary, tblAsset.CheckOut, tblMutasiDetail.LocID, tblMutasiDetail.LocName, tblMutasiDetail.Sat, tblMutasiDetail.Qty, tblMutasiDetail.Note, tblMutasi.TglBukti from tblAsset JOIN tblMutasiDetail ON tblMutasiDetail.AssetID=tblAsset.AssetID JOIN tblMutasi ON tblMutasi.NoBukti=tblMutasiDetail.NoBukti";
			$sql="SELECT tblAsset.AssetID, tblAsset.AssetName, tblAsset.NoSeri, tblAsset.Type, tblAsset.Merk, tblAsset.CheckOut,tblMutasiDetail.LocID, tblMutasiDetail.LocName, tblMutasi.TglBukti
			FROM tblMutasi INNER JOIN (tblAsset INNER JOIN tblMutasiDetail ON tblAsset.AssetID = tblMutasiDetail.AssetID) ON tblMutasi.NoBukti = tblMutasiDetail.NoBukti ";
		if($asset_type==''){
			if($location!=''){
			$sql.= "where tblMutasiDetail.LocID=$location ";
			}
		}else{
			$sql.= "where tblAsset.AssetTypeID=$asset_type ";
			if($location!=''){
			$sql.= "and tblMutasiDetail.LocID=$location";
			}
		}
		echo json_encode(array('sdfs'=>$sql));
	  $result = $db->query($sql);
	  $row = $result->fetchall();
		// exit;
		if(!$row){
			return 0;
		}
		else{
			return $row;
		}
	}

	function get_data($asset_type, $location){
		$html='';
		$tab=$this->get_row(100, $asset_type, $location);
		if($tab!=0){
		 foreach ($tab as $table) {
		    $html.="<tr>
		    <td>".$table['AssetID']."</td>
		    <td>".$table['AssetName']."</td>
		    <td>".$table['NoSeri']."</td>
		    <td>".$table['Type']."</td>
		    <td>".$table['Merk']."</td>
		    <td>".$table['LocID']."</td>
		    <td>".$table['LocName']."</td>
		    <td>".$table['TglBukti']."</td>
		  </tr>";
		  }
		}
		return $html;

	}

	function get_assets(){
		$dbName = 'E:/Asset Management/ModuleMtn.mdb';
		if (!file_exists($dbName)) {
				die("Could not find database file.");
		}
		$db = new PDO("odbc:DRIVER={Microsoft Access Driver (*.mdb)}; DBQ=$dbName; Uid=; Pwd=;");
		$sql  = "SELECT * from tblAssetType ";
		$result = $db->query($sql);
		$row = $result->fetchall();
		// print_r($row);
		return $row;
	}

	function get_location(){
		$dbName = 'E:/Asset Management/ModuleMtn.mdb';
		if (!file_exists($dbName)) {
				die("Could not find database file.");
		}
		$db = new PDO("odbc:DRIVER={Microsoft Access Driver (*.mdb)}; DBQ=$dbName; Uid=; Pwd=;");
		$sql  = "SELECT * from tblLokasi";
		$result = $db->query($sql);
		$row = $result->fetchall();
		return $row;
	}



}
